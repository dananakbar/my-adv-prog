package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected List<Spell> spells;
    protected List<Spell> reversedSpells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
        List<Spell> reversed = new ArrayList<Spell>();
        for (Spell spell : spells) {
            reversed.add(spell);
        }
        Collections.reverse(reversed);
        this.reversedSpells = reversed;
    }

    @Override
    public void cast(){
        for (Spell spell : this.spells) {
            spell.cast();
        }
    }

    @Override
    public void undo(){
        for (Spell spell : this.reversedSpells){
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
