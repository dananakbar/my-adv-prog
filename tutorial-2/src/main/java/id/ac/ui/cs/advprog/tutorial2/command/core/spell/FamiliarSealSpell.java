package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.*;

public class FamiliarSealSpell extends FamiliarSpell {
    // TODO: Complete Me
//    protected Familiar familiar;

    public FamiliarSealSpell(Familiar familiar) {
//        this.familiar = familiar;
        super(familiar);
    }

    @Override
    public void cast(){
        this.familiar.seal();
    }

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
