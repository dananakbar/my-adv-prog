package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    public String attack(){
        return "Launching a fireball";
    }

    public String getType(){
        return "Pearlwood Wand";
    }
}
