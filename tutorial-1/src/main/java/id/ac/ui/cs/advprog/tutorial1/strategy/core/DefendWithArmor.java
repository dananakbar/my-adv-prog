package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public String defend() {
        return "The armor is absorbing the attack";
    }

    public String getType() {
        return "Chainmail Armour Set";
    }
}
