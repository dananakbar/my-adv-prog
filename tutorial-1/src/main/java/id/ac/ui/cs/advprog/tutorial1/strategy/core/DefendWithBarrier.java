package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public String defend() {
        return "Covering in a bunker";
    }

    public String getType() {
        return "Sandsack Bunker";
    }
}
